@include('partials.front.header')

<body>
{{--<div class="page-loader"></div>--}}
<div class="content-wrapper">
    @include('partials.front.navbar')

    @yield('content')

    @include('partials.front.footer')

</div>

<div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
    </svg>
</div>

<script async src="{{ asset('js/plugins.js') }}"></script>
<script async src="{{ asset('js/theme.js') }}"></script>

<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "url": "https://www.telephonehelp.it",
      "logo": "https://www.telephonehelp.it/public/img/logo.png",
      "sameAs" : [
            "https://www.facebook.com/Telephonehelp",
            "https://www.instagram.com/telephonehelp/",
            "http://www.tiktok.com/@telephonehelp",
            "https://www.youtube.com/channel/UCBnTE9osZefdbt60NfNWKtQ/featured"
        ]
    }

</script>

@stack('footer-script')

<script type="text/javascript">
    var _iub = _iub || [];
    _iub.csConfiguration = {"ccpaAcknowledgeOnDisplay":true,"consentOnContinuedBrowsing":false,"countryDetection":true,"enableCcpa":true,"enableLgpd":true,"floatingPreferencesButtonDisplay":"anchored-center-right","invalidateConsentWithoutLog":true,"lgpdAppliesGlobally":false,"perPurposeConsent":true,"siteId":2838643,"whitelabel":false,"cookiePolicyId":90042367,"lang":"it", "banner":{ "acceptButtonDisplay":true,"closeButtonDisplay":false,"customizeButtonDisplay":true,"explicitWithdrawal":true,"listPurposes":true,"logo":"https://www.telephonehelp.it/img/telephonehelp_logo_negativo.png","position":"bottom","rejectButtonDisplay":true }};
</script>
<script type="text/javascript" data-src="//cdn.iubenda.com/cs/ccpa/stub.js" data-type='lazy-script'></script>
<script type="text/javascript" data-src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" data-type='lazy-script' async></script>

<script id='flying-scripts'>const loadScriptsTimer=setTimeout(loadScripts,10*1000);const userInteractionEvents=["mouseover","keydown","touchstart","touchmove","wheel"];userInteractionEvents.forEach(function(event){window.addEventListener(event,triggerScriptLoader,{passive:!0})});function triggerScriptLoader(){loadScripts();clearTimeout(loadScriptsTimer);userInteractionEvents.forEach(function(event){window.removeEventListener(event,triggerScriptLoader,{passive:!0})})}function loadScripts(){loadFacebook(); document.querySelectorAll("script,iframe[data-type='lazy-script']").forEach(function(elem){if(elem.hasAttribute('data-src')){ elem.setAttribute("src",elem.getAttribute("data-src"))}});}</script>

</body>
</html>
