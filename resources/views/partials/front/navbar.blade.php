<header class="wrapper bg-soft-primary">
    <nav class="navbar navbar-expand-lg classic transparent navbar-light">
        <div class="container flex-lg-row flex-nowrap align-items-center">
            <div class="navbar-brand w-100">
                <a href="{{ route('static') }}">
                    <img src="{{ asset('img/logo.png') }}" srcset="{{ asset('img/logo@2x.png') }} 2x" alt="Telephone Help" />
                </a>
            </div>
            <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start">
                <div class="offcanvas-header d-lg-none">
                    <h3 class="text-white fs-30 mb-0">Telephone Help</h3>
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body ms-lg-auto d-flex flex-column h-100">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" title="Home" href="{{ route('static') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" title="Chi siamo" href="{{ route('about') }}">Chi siamo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" title="Servizi" href="{{ route('services') }}">Servizi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" title="Galleria" href="{{ route('galleriesfront') }}">Galleria</a>
                        </li>
{{--                        <li class="nav-item dropdown">--}}
{{--                            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Servizi</a>--}}
{{--                            <ul class="dropdown-menu">--}}
{{--                                <li class="nav-item"><a class="dropdown-item" href="{{ route('assistence-smartphone') }}">Assistenza smartphone</a></li>--}}
{{--                                <li class="nav-item"><a class="dropdown-item" href="services2.html">Services II</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
                        <li class="nav-item">
                            <a class="nav-link" title="Contatti" href="{{ route('contact') }}">Contatti</a>
                        </li>
                    </ul>
                    <!-- /.navbar-nav -->
                    <div class="offcanvas-footer d-lg-none">
                        <div>
                            <a href="mailto:info@telephonehelp.it" class="link-inverse" target="_blank"><span class="__cf_email__">info@telephonehelp.it</span></a>
                            <br /> <a href="tel:390818626990" title="Chiamaci" target="_blank">081 862 69 90</a> <br />
                            <nav class="nav social social-white mt-4">
                                <a href="https://www.facebook.com/Telephonehelp" target="_blank" title="facebook"><i class="uil uil-facebook-f"></i></a>
                                <a href="https://www.instagram.com/telephonehelp/" target="_blank" title="instagram"><i class="uil uil-instagram"></i></a>
                                <a href="http://www.tiktok.com/@telephonehelp" target="_blank" title="tiktok"><svg fill="#fff" height="18" width="18" viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M448,209.91a210.06,210.06,0,0,1-122.77-39.25V349.38A162.55,162.55,0,1,1,185,188.31V278.2a74.62,74.62,0,1,0,52.23,71.18V0l88,0a121.18,121.18,0,0,0,1.86,22.17h0A122.18,122.18,0,0,0,381,102.39a121.43,121.43,0,0,0,67,20.14Z"/></svg></a>
                                <a href="https://www.youtube.com/channel/UCBnTE9osZefdbt60NfNWKtQ/featured" target="_blank" title="youtube"><i class="uil uil-youtube"></i></a>
                            </nav>
                            <!-- /.social -->
                        </div>
                    </div>
                    <!-- /.offcanvas-footer -->
                </div>
                <!-- /.offcanvas-body -->
            </div>
            <!-- /.navbar-collapse -->
            <div class="navbar-other ms-lg-4">
                <ul class="navbar-nav flex-row align-items-center ms-auto">
                    <li class="nav-item d-none d-md-block">
                        <a target="_blank" href="https://wa.me/+390818626990?text=Vorrei%20ricevere%20assistenza. Posso%20chattare%20con%20qualcuno?" class="btn btn-sm btn-primary rounded-pill">Richiedi Assistenza</a>
                    </li>
                    <li class="nav-item d-lg-none">
                        <button class="hamburger offcanvas-nav-btn"><span></span></button>
                    </li>
                </ul>
                <!-- /.navbar-nav -->
            </div>
            <!-- /.navbar-other -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- /.navbar -->
</header>
<!-- /header -->
