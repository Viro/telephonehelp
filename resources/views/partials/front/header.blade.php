<!DOCTYPE html>
<html lang="it">
<head>
    <title>{{ $meta_title }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ $meta_description }}">
    <meta name="author" content="Vincenzo Flauto">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

    <link rel="dns-prefetch" href="//www.google-analytics.com"/>
    <link rel="dns-prefetch" href="//www.facebook.com"/>
    <link rel="dns-prefetch" href="//imasdk.googleapis.com"/>

    <link rel="preconnect" href="//www.google-analytics.com"/>
    <link rel="preconnect" href="//www.facebook.com"/>

    <link rel="preload" href="{{ asset('css/plugins.css') }}" as="style"/>
    <link rel="preload" href="{{ asset('css/style.css') }}" as="style"/>
    <link rel="preload" href="{{ asset('css/app.css') }}" as="style"/>
    <link rel="preload" href="{{ asset('css/colors/sky.css') }}" as="style"/>

    <link rel="preload" href="{{ asset('js/plugins.js') }}" as="script"/>
    <link rel="preload" href="{{ asset('js/theme.js') }}" as="script"/>

    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/colors/sky.css') }}">
    <link rel="preload" href="{{ asset('css/fonts/urbanist.css') }}" as="style" onload="this.rel='stylesheet'">
    <link rel="canonical" href="{{ $canonical }}" />
    <meta property="fb:pages" content="61606836867"/>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-J127QXD0TB"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-J127QXD0TB');
    </script>

    <meta property="og:type" content="website" />
    <meta property="og:title" content="Telephone Help"/>
    <meta property="og:description" content="Telephone Help: Centro autorizzato IRP Apple. Siamo specializzati nelle riparazioni di cellulari, tablet e pc. Ripariamo cellulari apple, Samsung e tanti altri marchi."/>
    <meta property="og:url" content="https://www.telephonehelp.it"/>
    <meta property="og:image" content="https://www.telephonehelp.it/img/facebook_og.png"/>
    <meta property="og:image:alt" content="Telephone Help"/>
    <meta property="og:site_name" content="telephonehelp.it"/>
    <meta property="fb:app_id" content="555236749645933"/>

    @yield('content-header')

    <!-- Meta Pixel Code -->
    <script>
        function loadFacebook() {
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1705058519786026');
            fbq('track', 'PageView');
        }
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1705058519786026&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->
</head>
