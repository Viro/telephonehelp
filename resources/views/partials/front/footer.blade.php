<footer class="bg-navy text-inverse">
    <div class="container pt-12 pt-lg-6 pb-13 pb-md-15">
        <div class="row gy-6 gy-lg-0">
            <div class="col-md-4 col-lg-3">
                <div class="widget">
{{--                    <img class="mb-4" src="{{ asset('img/telephonehelp_logo_negativo.png') }}" srcset="{{ asset('img/telephonehelp_logo_negativo.png') }} 2x" alt="" />--}}
                    <p class="mb-4">© 2022 Telephone Help. <br class="d-none d-lg-block" />Tutti i diritti riservati.<br /><br />P.IVA 09838241215</p>
                    <nav class="nav social social-white">
                        <a href="https://www.facebook.com/Telephonehelp" target="_blank" title="facebook"><i class="uil uil-facebook-f"></i></a>
                        <a href="https://www.instagram.com/telephonehelp/" target="_blank" title="instagram"><i class="uil uil-instagram"></i></a>
                        <a href="http://www.tiktok.com/@telephonehelp" target="_blank" title="tiktok"><svg fill="#fff" height="18" width="18" viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M448,209.91a210.06,210.06,0,0,1-122.77-39.25V349.38A162.55,162.55,0,1,1,185,188.31V278.2a74.62,74.62,0,1,0,52.23,71.18V0l88,0a121.18,121.18,0,0,0,1.86,22.17h0A122.18,122.18,0,0,0,381,102.39a121.43,121.43,0,0,0,67,20.14Z"/></svg></a>
                        <a href="https://www.youtube.com/channel/UCBnTE9osZefdbt60NfNWKtQ/featured" target="_blank" title="youtube"><i class="uil uil-youtube"></i></a>
                    </nav>
                    <!-- /.social -->
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Dove siamo</h4>
                    <address class="pe-xl-15 pe-xxl-17">C.so Vittorio Emanuele III 348, 80058 - Torre Annunziata (Na)</address>
                    Chiamaci: <br /><a href="tel:+390818626990">+39 081 862 69 90</a>
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Informazioni</h4>
                    <ul class="list-unstyled  mb-0">
                        <li><a href="{{ route('about') }}" title="Chi siamo">Chi siamo</a></li>
                        <li><a href="{{ route('services') }}" title="Servizi">Servizi</a></li>
                        <li><a href="{{ route('contact') }}" title="Contatti">Contatti</a></li>
                        <li><a href="{{ route('cookie') }}" title="Cookie Policy">Cookie Policy</a></li>
                        <li><a href="{{ route('privacy') }}" title="Privaci Policy">Privacy Policy</a></li>
                    </ul>
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
            <div class="col-md-12 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Contattaci su Whatsapp</h4>
                    <p class="mb-5">Clicca sul pulsante per ricevere offerte, promozioni o assistenza.</p>
                    <div class="newsletter-wrapper">
                        <div id="mc_embed_signup2">
                            <a target="_blank" href="https://wa.me/+390818626990?text=Vorrei%20ricevere%20assistenza. Posso%20chattare%20con%20qualcuno?" class="btn btn-primary btn-icon btn-icon-start rounded">
                                <i class="uil uil-whatsapp"></i> Contattaci
                            </a>
                        </div>
                        <!--End mc_embed_signup-->
                    </div>
                    <!-- /.newsletter-wrapper -->
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->
        </div>
        <!--/.row -->
    </div>
    <!-- /.container -->
</footer>
