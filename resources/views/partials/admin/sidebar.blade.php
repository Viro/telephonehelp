<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="{{ asset('img/telephonehelp_logo_negativo.png') }}" alt="Telephone Help Logo" class="brand-image elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">&nbsp;</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
{{--            <div class="image">--}}
{{--                <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">--}}
{{--            </div>--}}
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name}} {{ Auth::user()->surname}}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
{{--        <div class="form-inline">--}}
{{--            <div class="input-group" data-widget="sidebar-search">--}}
{{--                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">--}}
{{--                <div class="input-group-append">--}}
{{--                    <button class="btn btn-sidebar">--}}
{{--                        <i class="fas fa-search fa-fw"></i>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link @if(isset($activeMenuHome) && $activeMenuHome == true) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-header">Pagine</li>
                @can('Vedi Pagine')
                    <li class="nav-item">
                        <a href="{{ route('pages.index') }}" class="nav-link @if(isset($activeMenuPages) && $activeMenuPages == true) active @endif">
                            <i class="nav-icon fas fa-list"></i>
                            <p>
                                Pagine
                            </p>
                        </a>
                    </li>
                @endcan
                <li class="nav-header">Video</li>
                @can('Vedi Galleria')
                    <li class="nav-item">
                        <a href="{{ route('galleries.index') }}" class="nav-link @if(isset($activeMenuGallery) && $activeMenuGallery == true) active @endif">
                            <i class="nav-icon fas fa-list"></i>
                            <p>
                                Video
                            </p>
                        </a>
                    </li>
                @endcan
                <li class="nav-header">{{__("app.settings")}}</li>
                @can('Vedi Utente')
                <li class="nav-item">
                    <a href="{{ route('users.index') }}" class="nav-link @if(isset($activeMenuUsers) && $activeMenuUsers == true) active @endif">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            {{__("app.users")}}
                        </p>
                    </a>
                </li>
                @endcan
                @can('Vedi Ruolo')
                <li class="nav-item">
                    <a href="{{ route('roles.index') }}" class="nav-link @if(isset($activeMenuRoles) && $activeMenuRoles == true) active @endif">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            {{__("app.roles")}}
                        </p>
                    </a>
                </li>
                @endcan
{{--                @can('Vedi Azienda')--}}
{{--                <li class="nav-item">--}}
{{--                    <a href="{{ route('companies.index') }}" class="nav-link @if(isset($activeMenuCompanies) && $activeMenuCompanies == true) active @endif">--}}
{{--                        <i class="nav-icon fas fa-house-user"></i>--}}
{{--                        <p>--}}
{{--                            {{__("app.companies")}}--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endcan--}}
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                <i class="nav-icon fas fa-sign-out-alt"></i> {{ __('Logout') }}
                            </button>
                        </form>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
