<footer class="main-footer">
    <strong>Copyright &copy; 2022 Vincenzo Flauto.</strong>
    Tutti i diritti riservati.
    <div class="float-right d-none d-sm-inline-block">
        <b>Versione</b> 2.0.1
    </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
