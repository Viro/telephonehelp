@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Modifica Pagina</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Modifica Pagina</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><a class="btn btn-sm btn-primary" href="{{ route('pages.index') }}"> {{__("app.back")}}</a></h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>

                <div class="card-body">
                    {!! Form::model($pages, ['method' => 'PATCH','route' => ['pages.update', $pages->id]]) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>{{__("app.name")}}:</strong>
                                {!! Form::text('title', $pages->title, array('placeholder' => 'Titolo','class' => 'form-control', 'disabled' => true)) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Slug:</strong>
                                {!! Form::text('slug', $pages->slug, array('placeholder' => 'Slug','class' => 'form-control', 'disabled' => true)) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Meta titolo:</strong>
                                {!! Form::text('meta_title', $pages->meta_title, array('placeholder' => 'Meta Titolo','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Meta descrizione:</strong>
                                {!! Form::text('meta_description', $pages->meta_description, array('placeholder' => 'Meta Descrizione','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Descrizione:</strong>
                                {!! Form::textarea('description', $pages->description, array('placeholder' => 'Descrizione','class' => 'form-control', 'rows' => 4, 'id' => 'summernote')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">{{__("app.submit")}}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

    </section>
@endsection

@push('footer-script')
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endpush
