@extends('layouts.app')

@section('content-header')
    {!! RecaptchaV3::initJs() !!}
@endsection

@section('content')
    <section class="wrapper image-wrapper bg-image bg-overlay bg-overlay-400 text-white" data-image-src="{{ asset('img/photos/bg3.jpg') }}">
        <div class="container pt-17 pb-20 pt-md-19 pb-md-21 text-center">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h1 class="display-1 mb-3 text-white">Mettiti in contatto con noi</h1>
                    <nav class="d-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb text-white">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contatti</li>
                        </ol>
                    </nav>
                    <!-- /nav -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light angled upper-end">
        <div class="container pb-11">
            <div class="row mb-14 mb-md-16">
                <div class="col-xl-10 mx-auto mt-n19">
                    <div class="card">
                        <div class="row gx-0">
                            <div class="col-lg-6 align-self-stretch">
                                <div class="map map-full rounded-top rounded-lg-start">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.2874084547843!2d14.441962132397677!3d40.755702976764695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x133ba369c65b75d5%3A0x2aa0498d0ee1ce50!2sTelephone%20Help%20-%20Centro%20Autorizzato%20Apple%20IRP!5e0!3m2!1sit!2sit!4v1666389996056!5m2!1sit!2sit" style="width:100%; height: 100%; border:0" allowfullscreen></iframe>
                                </div>
                                <!-- /.map -->
                            </div>
                            <!--/column -->
                            <div class="col-lg-6">
                                <div class="p-10 p-md-11 p-lg-14">
                                    <div class="d-flex flex-row">
                                        <div>
                                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-location-pin-alt"></i> </div>
                                        </div>
                                        <div class="align-self-start justify-content-start">
                                            <h5 class="mb-1">Indirizzo</h5>
                                            <address>C.so V. Emanuele III, 348<br class="d-none d-md-block" />80058 - Torre Annunziata (Na)</address>
                                        </div>
                                    </div>
                                    <!--/div -->
                                    <div class="d-flex flex-row">
                                        <div>
                                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-phone-volume"></i> </div>
                                        </div>
                                        <div>
                                            <h5 class="mb-1">Telefono</h5>
                                            <p><a href="tel:390818626990" target="_blank" title="Chiamaci subito">081 862 69 90</a></p>
                                        </div>
                                    </div>
                                    <!--/div -->
                                    <div class="d-flex flex-row">
                                        <div>
                                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-whatsapp"></i> </div>
                                        </div>
                                        <div>
                                            <h5 class="mb-1">WhatsApp</h5>
                                            <p><a target="_blank" href="https://wa.me/+390818626990?text=Vorrei%20ricevere%20assistenza. Posso%20chattare%20con%20qualcuno?" title="Contattaci su WhatsApp">081 862 69 90</a></p>
                                        </div>
                                    </div>
                                    <!--/div -->
                                    <div class="d-flex flex-row">
                                        <div>
                                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-envelope"></i> </div>
                                        </div>
                                        <div>
                                            <h5 class="mb-1">E-mail</h5>
                                            <p class="mb-0"><a href="mailto:info@telephonehelp.it" title="Invia una mail a Telephone Help" class="link-body">info@telephonehelp.it</a></p>
                                        </div>
                                    </div>
                                    <!--/div -->
                                </div>
                                <!--/div -->
                            </div>
                            <!--/column -->
                        </div>
                        <!--/.row -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                    <h2 class="display-4 mb-3 text-center">Compila il form</h2>
                    <p class="lead text-center mb-10">Contattaci tramite il modulo di contatto qui sotto e ti ricontatteremo a breve.</p>

                    @if(Session::has('success'))
                        <div class="messages">
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        </div>
                    @endif

                    <form method="post" action="{{ route('contact.store') }}">
                        {{ csrf_field() }}
                        <div class="row gx-4">
                            <div class="col-md-6">
                                <div class="form-floating mb-4">
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Jane" required>
                                    <label for="form_name">Nome Completo *</label>
{{--                                    <div class="valid-feedback"> Looks good! </div>--}}
{{--                                    <div class="invalid-feedback"> Inserisci il nome completo. </div>--}}
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-md-6">
                                <div class="form-floating mb-4">
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="jane.doe@example.com" required>
                                    <label for="form_email">Email *</label>
{{--                                    <div class="valid-feedback"> Looks good! </div>--}}
{{--                                    <div class="invalid-feedback"> Inserisci una mail valida. </div>--}}
                                    @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-12">
                                <div class="form-floating mb-4">
                                    <textarea id="form_message" name="message" class="form-control" placeholder="Il tuo messaggio" style="height: 150px" required></textarea>
                                    <label for="form_message">Messaggio *</label>
{{--                                    <div class="valid-feedback"> Looks good! </div>--}}
{{--                                    <div class="invalid-feedback"> Inserisci un messaggio. </div>--}}
                                    @if ($errors->has('message'))
                                        <span class="text-danger">{{ $errors->first('message') }}</span>
                                    @endif
                                </div>
                            </div>
                            <!-- /column -->
                            <div class="col-12 text-center">
                                <input type="submit" class="btn btn-primary rounded-pill btn-send mb-3" value="Invia messaggio">
                                <p class="text-muted"><strong>*</strong> Tutti i campi sono richiesti.</p>
                            </div>
                            <!-- /column -->

                            <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {!! RecaptchaV3::field('register') !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!-- /.row -->
                    </form>
                    <!-- /form -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
