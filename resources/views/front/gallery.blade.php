@extends('layouts.app')

@section('content')
    <section class="wrapper bg-gray">
        <div class="container pt-12 pt-md-16 text-center">
            <div class="row">
                <div class="col-lg-8 col-xxl-7 mx-auto text-center" data-cues="slideInDown" data-group="page-title" data-delay="600">
                    <h2 class="fs-16 text-uppercase ls-xl text-dark mb-4">Seguici sul nostro canale YouTube</h2>
                    <h1 class="display-1 fs-58 mb-7">Esplora la nostra galleria video.</h1>
                    @if ($show_principal)
                    <div class="d-flex justify-content-center" data-cues="slideInDown" data-group="page-title-buttons" data-delay="900">
                        <span><a href="#video-location" class="btn btn-lg btn-primary rounded-pill me-2">Vai ai video</a></span>
                    </div>
                    @endif
                </div>
                <!--/column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        @if ($show_principal)
        <figure class="position-absoute" style="bottom: 0; left: 0; z-index: 2;"><img src="{{ asset('img/photos/bg11.jpg') }}" alt="Vedi i nostri video su YouTube" /></figure>
        @endif
    </section>
    <!-- /section -->
    <section class="wrapper bg-gray">
        <div class="container">
            <div class="card shadow-none mb-0">
                <div class="card-body py-12 py-lg-14 px-lg-11 py-xl-16 px-xl-13" id="video-location">
                    <div class="row gx-md-8 gx-xl-12 gy-8 mb-14 mb-md-16 text-center">
                        @foreach($galleries as $gallery)
                        <div class="col-md-6">
                            <div class="player" data-plyr-provider="youtube" data-plyr-embed-id="{{ $gallery->code }}"></div>
                        </div>
                        @endforeach
                        <!--/column -->
                    </div>
                    <!--/.row -->

                    {{ $galleries->links() }}

                </div>
                <!--/.card-body -->
            </div>
            <!--/.card -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
