@extends('layouts.app')

@section('content')
    <section class="wrapper bg-soft-primary">
        <div class="container pt-10 pt-lg-12 pt-xl-12 pt-xxl-10 pb-lg-10 pb-xl-10 pb-xxl-0">
            <div class="row gx-md-8 gx-xl-12 gy-10 align-items-center text-center text-lg-start">
                <div class="col-lg-6" data-cues="slideInDown" data-group="page-title" data-delay="900">
                    <h1 class="display-1 mb-4 me-xl-5 mt-lg-n10">Assistenza e supporto da  <br class="d-none d-md-block d-lg-none" /><span class="text-primary">Personale qualificato</span></h1>
                    <p class="lead fs-24 lh-sm mb-7 pe-xxl-15">I nostri tecnici sono specilizzati nella riparazione di <br class="d-none d-md-block d-lg-none" /> smartphone, tablet e pc.
                        <br class="d-none d-md-block d-lg-none" />Contattaci o vieni direttamente nel nostro store.</p>
                    <div class="d-inline-flex me-2"><a href="https://wa.me/+390818626990?text=Vorrei%20ricevere%20assistenza. Posso%20chattare%20con%20qualcuno?" target="_blank" class="btn btn-lg btn-grape rounded">Contattaci Subito</a></div>
                </div>
                <!--/column -->
                <div class="col-10 col-md-7 mx-auto col-lg-6 col-xl-5 ms-xl-5">
                    <img class="img-fluid mb-n12 mb-md-n14 mb-lg-n19" src="{{ asset('img/illustrations/3d11.png') }}" srcset="{{ asset('img/illustrations/3d11.png') }} 2x" data-cue="fadeIn" data-delay="300" alt="Assistenza e supporto da Personale qualificato" />
                </div>
                <!--/column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        <figure><img src="{{ asset('img/photos/clouds.png') }}" alt="nuvole"></figure>
    </section>
    <!-- /section -->
    <section class="wrapper bg-white">
        <div class="container pt-15 pb-15 pb-md-17">
            <div class="row text-center">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                    <h2 class="fs-16 text-uppercase text-primary mb-3">Cosa Facciamo?</h2>
                    <h3 class="display-3 mb-10 px-xxl-10">Siamo specializzati nella riparazione delle seguenti tipologie di prodotti.</h3>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="row gx-md-8 gy-8 mb-15 mb-md-17 text-center">
                <div class="col-lg-4">
                    <div class="px-md-15 px-lg-3">
                        <figure class="mb-6"><img class="img-fluid" src="{{ asset('img/illustrations/i25.png') }}" srcset="{{ asset('img/illustrations/i25.png') }} 2x" alt="pc e computer portatili" /></figure>
                        <h3>PC</h3>
                        <p class="mb-2">Ripariamo PC di qualsiasi brand. Ogni nostra riparazione viene gestita con massima professionalità e discrezione.</p>
                    </div>
                    <!--/.px -->
                </div>
                <!--/column -->
                <div class="col-lg-4">
                    <div class="px-md-15 px-lg-3">
                        <figure class="mb-6"><img class="img-fluid" src="{{ asset('img/illustrations/i26.png') }}" srcset="{{ asset('img/illustrations/i26.png') }} 2x" alt="riparazioni smartphone apple, samsung, huawei ed altri brand" /></figure>
                        <h3>Smartphone</h3>
                        <p class="mb-2">Ripariamo smartphone di svariati brand. Siamo specializzati in smartphone apple, samsgung, huawei e tanti altri brand...</p>
                    </div>
                    <!--/.px -->
                </div>
                <!--/column -->
                <div class="col-lg-4">
                    <div class="px-md-15 px-lg-3">
                        <figure class="mb-6"><img class="img-fluid" src="{{ asset('img/illustrations/i16.png') }}" srcset="{{ asset('img/illustrations/i16.png') }} 2x" alt="riparazioni tablet apple, samsgung, huawei e tanti altri brand..." /></figure>
                        <h3>Tablet</h3>
                        <p class="mb-2">Ripariamo tablet di svariati brand. Siamo specializzati in tablet apple, samsgung e tanti altri....</p>
                    </div>
                    <!--/.px -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
            <div class="row gx-3 gy-10 mb-14 mb-md-16 align-items-center">
                <div class="col-lg-6">
                    <figure><img class="w-auto" src="{{ asset('img/illustrations/3d2.png') }}" srcset="{{ asset('img/illustrations/3d2@2x.png') }} 2x" alt="Alcune ragioni per cui altri clienti ci hanno scelto" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5 ms-auto">
                    <h2 class="fs-16 text-uppercase text-grape mb-3">Perchè Sceglierci?</h2>
                    <h3 class="display-4 mb-8">Alcune ragioni per cui altri clienti ci hanno scelto.</h3>
                    <div class="row gy-6">
                        <div class="col-md-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <img src="{{ asset('img/icons/solid/monitor.svg') }}" class="svg-inject icon-svg icon-svg-xs solid-mono text-grape me-4" alt="Professionali" />
                                </div>
                                <div>
                                    <h4 class="mb-1">Professionali</h4>
                                    <p class="mb-0">La nostra missione è la soddisfazione del cliente.</p>
                                </div>
                            </div>
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <img src="{{ asset('img/icons/solid/verify.svg') }}" class="svg-inject icon-svg icon-svg-xs solid-mono text-grape me-4" alt="Garanzia" />
                                </div>
                                <div>
                                    <h4 class="mb-1">Garanzia</h4>
                                    <p class="mb-0">Garanzia fino ad un anno.</p>
                                </div>
                            </div>
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <img src="{{ asset('img/icons/solid/secure.svg') }}" class="svg-inject icon-svg icon-svg-xs solid-mono text-grape me-4" alt="Sicurezza" />
                                </div>
                                <div>
                                    <h4 class="mb-1">Sicurezza</h4>
                                    <p class="mb-0">Con noi, i tuoi dati sono sempre al sicuro.</p>
                                </div>
                            </div>
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <img src="{{ asset('img/icons/solid/headphone.svg') }}" class="svg-inject icon-svg icon-svg-xs solid-mono text-grape me-4" alt="Supporto" />
                                </div>
                                <div>
                                    <h4 class="mb-1">Supporto pre e post vendita</h4>
                                    <p class="mb-0">Non ti lasciamo mai solo.</p>
                                </div>
                            </div>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
            <div class="row gx-3 gy-10 gy-lg-0 mb-15 mb-md-17 align-items-center">
                <div class="col-lg-5 mx-auto order-lg-2">
                    <figure><img class="w-auto" src="{{ asset('img/illustrations/3d5.png') }}" srcset="{{ asset('img/illustrations/3d5@2x.png') }} 2x" alt="Telephone Help, centro Centro autorizzato IRP Apple" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5 me-auto">
                    <h2 class="fs-16 text-uppercase text-grape mb-3">Altro su di noi</h2>
                    <h3 class="display-4 mb-5 pe-xxl-5">Siamo un centro Centro autorizzato IRP Apple.</h3>
                    <p class="mb-6"><strong>Telephone Help</strong> ha aderito al programma
                        <a href="https://support.apple.com/it-it/repair/verify-repair-provider?storeid=I6543841" target="_blank">
                            Apple Independent Repair Provider</a>, di conseguenza possiamo fornire servizi di riparazione fuori garanzia su prodotti <strong>Apple iPhone e Mac</strong>.
                        Grazie a questo accordo utilizziamo <strong>ricambi originali</strong> forniti direttamente da <strong>Apple</strong>. Utilizziamo tutti gli strumenti, le diagnostiche e
                        le <strong>procedure di Apple</strong> per garantire <strong>riparazioni Apple</strong> sicure e affidabili.
                    </p>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="wrapper bg-light upper-end lower-end">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 mb-14 mb-md-17 align-items-center">
                <div class="col-lg-6 position-relative order-lg-2">
                    <div class="shape bg-dot primary rellax w-16 h-20" data-rellax-speed="1" style="top: 3rem; left: 5.5rem"></div>
                    <div class="overlap-grid overlap-grid-2">
                        <div class="item">
                            <figure class="rounded shadow"><img src="{{ asset('img/illustrations/assistenza1.png') }}" srcset="{{ asset('img/illustrations/assistenza1.png') }} 2x" alt=""></figure>
                        </div>
                        <div class="item">
                            <figure class="rounded shadow"><img src="{{ asset('img/illustrations/assistenza2.png') }}" srcset="{{ asset('img/illustrations/assistenza2.png') }} 2x" alt=""></figure>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <img src="{{ asset('img/icons/lineal/megaphone.svg') }}" class="svg-inject icon-svg icon-svg-md mb-4" alt="" />
                    <h2 class="display-4 mb-3">Cosa facciamo in dettaglio</h2>
                    <p class="lead fs-lg">Di seguito la lista dei servizi di riparazione che offriamo.</p>
                    <div class="row gy-3 gx-xl-8">
                        <div class="col-xl-6">
                            <ul class="icon-list bullet-bg bullet-soft-primary mb-0">
                                <li><span><i class="uil uil-check"></i></span><span>Sostituzione display</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Sostituzione batterie</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Rigenerazione (sostituzione del solo vetro) smartphone, tablet e apple watch</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Riparazione hardware e software smartphone e tablet</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Riparazioni hardware macbook e notebook</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Backup in tutti i modelli di smartphone</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Micro saldature su schede connettori</span></li>
                            </ul>
                        </div>
                        <!--/column -->
                        <div class="col-xl-6">
                            <ul class="icon-list bullet-bg bullet-soft-primary mb-0">
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Sostituzione di tutti i tipi di pulsanti</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Sostituzione speaker altoparlante iphone</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Sostituzione microfono</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Sostituzione tasto home iphone</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Sostituzione sensore prossimita iphone</span></li>
                                <li class="mt-3"><span><i class="uil uil-check"></i></span><span>Sostituzione cover posteriore iphone</span></li>
                            </ul>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
