@extends('layouts.app')

@section('content')
    <section class="">
        <div class="container pt-10 text-center">
            <div class="row">
                <div class="col-xl-5 mx-auto mb-6">
                    <h1 class="display-1 mb-3">Chi siamo</h1>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section>
        <div class="container py-4">
            <div class="row gx-lg-12 gx-xl-12 gy-10 mb-14 mb-md-17 align-items-center">
                <div class="col-lg-12">
                    <p class="lead text-left">
                        Siamo un laboratorio che esegue riparazioni su <strong>smartphone, tablet e PC</strong> di qualsiasi marca e modello.
                        <br /><br />
                        Le nostre riparazioni sono effettuate in tempi brevi da <strong>tecnici specializzati</strong> che le eseguono con attrezzature
                        innovative di ultima generazione. Ci aggiorniamo continuamente sulle nuove tecnologie per offrire un servizio altamente specializzato.
                        Per le nostre riparazioni utilizziamo solo <strong>ricambi di qualità!</strong>
                        Siamo in grado di effettuare <strong>riparazioni</strong> e <strong>microsaldature</strong> su <strong>schede madri</strong>, garantendo la risoluzione di molteplici problematiche evidenziate dai clienti.
                        <br /><br />
                        Inoltre il nostro laboratorio ha aderito al programma <a href="https://support.apple.com/it-it/repair/verify-repair-provider?storeid=I6543841" target="_blank">
                            Apple Independent Repair Provider</a>, di conseguenza possiamo fornire servizi di riparazione fuori garanzia su prodotti <strong>Apple iPhone e Mac</strong>.
                        Grazie a questo accordo utilizziamo <strong>ricambi originali</strong> forniti direttamente da <strong>Apple</strong>. Utilizziamo tutti gli strumenti, le diagnostiche e
                        le <strong>procedure di Apple</strong> per garantire <strong>riparazioni Apple</strong> sicure e affidabili.
                    </p>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="wrapper bg-light upper-end lower-end">
        <div class="container py-14 py-md-10">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-7">
                    <figure><img class="w-auto" src="{{ asset('img/illustrations/i5.png') }}" srcset="{{ asset('img/illustrations/i5@2x.png') }} 2x" alt="" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5">
                    <h2 class="fs-15 text-uppercase text-line text-primary text-center mb-3">Mettiti in contatto</h2>
                    <h3 class="display-5 mb-7">Hai qualche domanda? Non esitare a contattarci.</h3>
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-location-pin-alt"></i> </div>
                        </div>
                        <div>
                            <h5 class="mb-1">Indirizzo</h5>
                            <address>C.so V. Emanuele III, 348, 80058, Torre Annunziata (Na)</address>
                        </div>
                    </div>
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-phone-volume"></i> </div>
                        </div>
                        <div>
                            <h5 class="mb-1">Telefono</h5>
                            <p>081 862 69 90</p>
                        </div>
                    </div>
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-whatsapp"></i> </div>
                        </div>
                        <div>
                            <h5 class="mb-1">WhatsApp</h5>
                            <p>081 862 69 90</p>
                        </div>
                    </div>
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon text-primary fs-28 me-4 mt-n1"> <i class="uil uil-envelope"></i> </div>
                        </div>
                        <div>
                            <h5 class="mb-1">E-mail</h5>
                            <p class="mb-0"><a href="mailto:info@telephonehelp.it" class="link-body">info@telephonehelp.it</a></p>
                        </div>
                    </div>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
