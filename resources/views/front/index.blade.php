@extends('layouts.app')

@section('content')
<section class="wrapper bg-light">
    <div class="container pt-10 pt-md-8 pb-14 pb-md-4 text-center">
        <div class="row gx-lg-8 gx-xl-12 gy-10 gy-xl-0 mb-4 align-items-center">
            <div class="col-lg-7 order-lg-2">
                <figure><img class="img-auto" src="{{ asset('img/illustrations/i26.png') }}" srcset="{{ asset('img/illustrations/i26.png') }} 2x" alt="" /></figure>
            </div>
            <!-- /column -->
            <div class="col-md-10 offset-md-1 offset-lg-0 col-lg-5 text-center text-lg-start">
                <h1 class="display-1 fs-54 mb-5 mx-md-n5 mx-lg-0 mt-7">Affidati a noi <br class="d-md-none">per le tue riparazioni di <br class="d-md-none"><span class="rotator-fade text-primary">smartphone,tablet,computer</span></h1>
                <p class="lead fs-lg mb-7">Siamo specializzati nelle riparazioni di cellulari, tablet e pc. Ripariamo cellulari apple, samsung e tanti altri brand.</p>
                <span><a href="https://wa.me/+390818626990?text=Vorrei%20ricevere%20assistenza. Posso%20chattare%20con%20qualcuno?" target="_blank" class="btn btn-lg btn-primary rounded-pill me-2">Contattaci subito</a></span>
            </div>
            <!-- /column -->
        </div>
        <!-- /.row -->
{{--        <p class="text-center mb-8">Specializzati con i seguenti brand</p>--}}
{{--        <div class="row row-cols-4 row-cols-md-4 row-cols-lg-7 row-cols-xl-7 gy-10 mb-2 d-flex align-items-center justify-content-center">--}}
{{--            <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{ asset('img/brands/c1.png') }}" alt="" /></div>--}}
{{--            <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{ asset('img/brands/c2.png') }}" alt="" /></div>--}}
{{--            <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{ asset('img/brands/c3.png') }}" alt="" /></div>--}}
{{--            <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{ asset('img/brands/c4.png') }}" alt="" /></div>--}}
{{--            <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{ asset('img/brands/c5.png') }}" alt="" /></div>--}}
{{--            <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{ asset('img/brands/c6.png') }}" alt="" /></div>--}}
{{--            <div class="col"><img class="img-fluid px-md-3 px-lg-0 px-xl-2 px-xxl-5" src="{{ asset('img/brands/c7.png') }}" alt="" /></div>--}}
{{--        </div>--}}
{{--        <!-- /.row -->--}}
    </div>
    <!-- /.container -->
    <div class="overflow-hidden">
        <div class="divider text-soft-primary mx-n2">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100"><path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z"/></svg>
        </div>
    </div>
</section>
<!-- /section -->
    <section class="wrapper bg-gradient-primary">
        <div class="container pt-12 pt-lg-4 pb-14 pb-md-17">
            <div class="row text-center">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                    <h2 class="fs-16 text-uppercase text-primary mb-3">Cosa Facciamo?</h2>
                    <h3 class="display-3 mb-10 px-xxl-10">Siamo specializzati nella riparazione delle seguenti tipologie di prodotti.</h3>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="row gx-lg-8 gx-xl-12 gy-11 px-xxl-5 text-center d-flex align-items-end">
                <div class="col-lg-4">
                    <div class="px-md-15 px-lg-3">
                        <figure class="mb-6"><img class="img-fluid" src="{{ asset('img/illustrations/i25.png') }}" srcset="{{ asset('img/illustrations/i25.png') }} 2x" alt="" /></figure>
                        <h3>PC</h3>
                        <p class="mb-2">Ripariamo PC di qualsiasi brand. Ogni nostra riparazione viene gestita con massima professionalità e discrezione.</p>
                    </div>
                    <!--/.px -->
                </div>
                <!--/column -->
                <div class="col-lg-4">
                    <div class="px-md-15 px-lg-3">
                        <figure class="mb-6"><img class="img-fluid" src="{{ asset('img/illustrations/i26.png') }}" srcset="{{ asset('img/illustrations/i26.png') }} 2x" alt="" /></figure>
                        <h3>Smartphone</h3>
                        <p class="mb-2">Ripariamo smartphone di svariati brand. Siamo specializzati in smartphone apple, samsgung, huawei e tanti altri....</p>
                    </div>
                    <!--/.px -->
                </div>
                <!--/column -->
                <div class="col-lg-4">
                    <div class="px-md-15 px-lg-3">
                        <figure class="mb-6"><img class="img-fluid" src="{{ asset('img/illustrations/i16.png') }}" srcset="{{ asset('img/illustrations/i16.png') }} 2x" alt="" /></figure>
                        <h3>Tablet</h3>
                        <p class="mb-2">Ripariamo tablet di svariati brand. Siamo specializzati in tablet apple, samsgung, huawei e tanti altri....</p>
                    </div>
                    <!--/.px -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container pb-14 pb-md-17">
            <div class="row gx-md-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-6 order-lg-2">
                    <div class="card shadow-lg me-lg-6">
                        <div class="card-body p-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <span class="icon btn btn-circle btn-lg btn-soft-primary pe-none me-4"><span class="number">01</span></span>
                                </div>
                                <div>
                                    <h4 class="mb-1">Preventivo</h4>
                                    <p class="mb-0">Preventivare al cliente la riparazione da effettuare.</p>
                                </div>
                            </div>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                    <div class="card shadow-lg ms-lg-13 mt-6">
                        <div class="card-body p-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <span class="icon btn btn-circle btn-lg btn-soft-primary pe-none me-4"><span class="number">02</span></span>
                                </div>
                                <div>
                                    <h4 class="mb-1">Riparazione</h4>
                                    <p class="mb-0">Provvederemo alla riparazione del prodotto.</p>
                                </div>
                            </div>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                    <div class="card shadow-lg mx-lg-6 mt-6">
                        <div class="card-body p-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <span class="icon btn btn-circle btn-lg btn-soft-primary pe-none me-4"><span class="number">03</span></span>
                                </div>
                                <div>
                                    <h4 class="mb-1">Finalizzazione Prodotto</h4>
                                    <p class="mb-0">Test e certificazione del prodotto per l'avvenuta riparazione.</p>
                                </div>
                            </div>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <h2 class="fs-16 text-uppercase text-primary mb-3">Flusso di lavoro</h2>
                    <h3 class="display-3 mb-4">Il nostro flusso di lavoro si suddivide in tre step.</h3>
                    <p>Ogni fase del nostro flusso di lavoro è seguita attentamente da un professionista del settore. Nessun aspetto viene lasciato al caso, la professionalità e la soddisfazione del cliente viene prima di tutto.</p>
                    <p class="mb-6">Il primo passo è quello di visionare e preventivare ogni prodotto che viene inviato al nostro laboratorio, dopo una prima fase di preventivazione
                        procediamo alla seconda fase, dove provvediamo, previa accettazione del preventivo, alla riparazione del prodotto.
                        La terza fase sussiste nel testare attentamente il prodotto e certificare la riparazione avvenuta con successo.</p>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-gradient-reverse-primary">
        <div class="container pb-14 pb-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-7">
                    <figure><img class="img-auto" src="{{ asset('img/illustrations/i3.png') }}" srcset="{{ asset('img/illustrations/i3.png') }} 2x" alt="" /></figure>
                </div>
                <!--/column -->
                <div class="col-lg-5">
                    <h2 class="fs-15 text-uppercase text-primary mb-3">Perchè sceglierci?</h2>
                    <h3 class="display-3 mb-7">Offriamo soluzioni ai tuoi problemi.</h3>
                    <div class="accordion accordion-wrapper" id="accordionExample">
                        <div class="card plain accordion-item">
                            <div class="card-header" id="headingOne">
                                <button class="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Professionali </button>
                            </div>
                            <!--/.card-header -->
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>La nostra missione è la soddisfazione del cliente, non trascuriamo nessun aspetto durante il nostro lavoro.</p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                        <div class="card plain accordion-item">
                            <div class="card-header" id="headingTwo">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Garanzia </button>
                            </div>
                            <!--/.card-header -->
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Ogni nostra riparazione è coperta da una garanzia che può arrivare fino ad un anno. In fase di preventivo comunichiamo la garanzia offerta in base all'intervento effettuato.</p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                        <div class="card plain accordion-item">
                            <div class="card-header" id="headingThree">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> IRP Apple </button>
                            </div>
                            <!--/.card-header -->
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Siamo un centro certificato IRP Apple.</p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                    </div>
                    <!--/.accordion -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
        <div class="overflow-hidden">
            <div class="divider text-light mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.overflow-hidden -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-gradient-primary">
        <div class="container pt-12 pt-lg-8 pb-14 pb-md-17">
            <div class="row text-center">
                <div class="col-lg-8 offset-lg-2">
                    <h2 class="fs-16 text-uppercase text-primary mb-3">Recensioni</h2>
                    <h3 class="display-3 mb-10 px-xxl-10">Non prendere sul serio quanto descritto sopra. Guarda i nostri clienti cosa dicono di noi.</h3>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="grid">
                <div class="row isotope gy-6">
                    @if(count($reviews) > 0)
                    @foreach($reviews as $review)
                    <div class="item col-md-6 col-xl-4">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <span class="ratings {{ $review['rating'] }} mb-2"></span>
                                <blockquote class="border-0 mb-0">
                                    <p>{{ $review['text'] }}</p>
                                    <div class="blockquote-details">
                                        <img class="rounded-circle w-12" src="{{ $review['profile_photo_url'] }}" srcset="{{ $review['profile_photo_url'] }} 2x" alt="{{ $review['author_name'] }}" />
                                        <div class="info">
                                            <h5 class="mb-1">{{ $review['author_name'] }}</h5>
                                            <p class="mb-0">{{ $review['relative_time_description'] }}</p>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!--/column -->
                    @endforeach
                    @endif
                </div>
                <!-- /.row -->
            </div>
            <!-- /.grid-view -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
