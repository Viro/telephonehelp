<?php

return array (
    'roles' =>
        array (
            'deletesure' => 'Non sarai in grado di recuperare il ruolo eliminato!',
        ),
    'users' =>
        array (
            'deletesure'  =>  'Non sarai in grado di recuperare l\'utente eliminato!',
        ),
    'permission' =>
        array (
            'viewMember'  =>  'Vedi Membri',
        ),
    'companies' =>
        array (
            'deletesure'  =>  'Non sarai in grado di recuperare l\'azienda eliminato!',
        ),
);
