<?php

return [
    'message' => 'Questo sito utilizza cookie tecnici e analitici, per maggiori informazioni consultare la pagina <a href="'.route('cookie').'" title="Coockie Policy">COOKIE POLICY</a>.',
    'agree' => 'Consenti i cookies',
];
