<?php

return array (
    'roles' =>
        array (
            'deletesure' => 'You will not be able to recover the deleted role!',
        ),
    'users' =>
        array (
            'deletesure'  =>  'You will not be able to recover the deleted user!',
        ),
    'permission' =>
        array (
            'viewMember'  =>  'View Member',
        ),
    'companies' =>
        array (
            'deletesure'  =>  'You will not be able to recover the deleted company!',
        ),
);
