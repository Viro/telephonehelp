<?php

//per la cache
//https://github.com/JosephSilber/page-cache

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\StaticController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\GalleriesController;
use App\Http\Controllers\ContactController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::middleware('page-cache')->get('/', [StaticController::class, 'index'])->name('static');
Route::middleware('page-cache')->get('/chi-siamo', [StaticController::class, 'about'])->name('about');
Route::middleware('page-cache')->get('/richiedi-assistenza', [StaticController::class, 'assistence'])->name('assistence');
Route::middleware('page-cache')->get('/cookie-policy', [StaticController::class, 'cookie'])->name('cookie');
Route::middleware('page-cache')->get('/privacy-policy', [StaticController::class, 'privacy'])->name('privacy');
Route::middleware('page-cache')->get('/servizi', [StaticController::class, 'services'])->name('services');
Route::middleware('page-cache')->get('/galleria', [StaticController::class, 'gallery'])->name('galleriesfront');

//Route::get('/', [StaticController::class, 'index'])->name('static');
//Route::get('/chi-siamo', [StaticController::class, 'about'])->name('about');
//Route::get('/richiedi-assistenza', [StaticController::class, 'assistence'])->name('assistence');
//Route::get('/cookie-policy', [StaticController::class, 'cookie'])->name('cookie');
//Route::get('/privacy-policy', [StaticController::class, 'privacy'])->name('privacy');
//Route::get('/servizi', [StaticController::class, 'services'])->name('services');
//Route::get('/galleria', [StaticController::class, 'gallery'])->name('galleriesfront');

//Route::middleware('page-cache')->get('/contatti', [StaticController::class, 'contact'])->name('contact');

//Route::get('/', [StaticController::class, 'index'])->name('static');
//Route::get('/chi-siamo', [StaticController::class, 'about'])->name('about');
//Route::get('/richiedi-assistenza', [StaticController::class, 'assistence'])->name('assistence');
//Route::get('/cookie-policy', [StaticController::class, 'cookie'])->name('cookie');
//Route::get('/privacy-policy', [StaticController::class, 'privacy'])->name('privacy');

//Route::get('/servizi', [StaticController::class, 'services'])->name('services');
//Route::get('/galleria', [StaticController::class, 'gallery'])->name('galleriesfront');

Route::get('/contatti', [StaticController::class, 'contact'])->name('contact');
Route::post('/contatti', [ContactController::class, 'store'])->name('contact.store');


//Route::get('/login', [AdminController::class, 'index'])->name('login');

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {

    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    Route::get('/users/data/', [UserController::class, 'data'])->name('users.data');
    Route::resource('users', UserController::class);

    //roles
    Route::get('/roles/data/', [RoleController::class, 'data'])->name('roles.data');
    Route::resource('roles', RoleController::class);

    //pages
    Route::get('/pages/data/', [PagesController::class, 'data'])->name('pages.data');
    Route::resource('pages', PagesController::class);

    //galleries
    Route::get('/galleries/data/', [GalleriesController::class, 'data'])->name('galleries.data');
    Route::resource('galleries', GalleriesController::class);
});

Auth::routes();
