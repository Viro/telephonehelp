<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Vincenzo';
        $user->surname = 'Flauto';
        $user->email = 'flauto.vincenzo84@gmail.com';
        $user->password = Hash::make('123456');
        $user->save();
        $user->assignRole('superadmin');

        $user2 = new User();
        $user2->name = 'Utente';
        $user2->surname = 'Primo';
        $user2->email = 'test@test.it';
        $user2->password = Hash::make('123456');
        $user2->save();
        $user2->assignRole('admin');
    }
}
