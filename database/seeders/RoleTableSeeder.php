<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $super = Role::create([
            'name' => 'superadmin'
        ]);

        $admin = Role::create([
            'name' => 'admin'
        ]);

        $employee = Role::create([
            'name' => 'employee'
        ]);

        $client = Role::create([
            'name' => 'client'
        ]);

        $permissions = Permission::orderBy('module_id')->orderBy('name')->pluck('name');
        foreach($permissions as $perm){
            $super->givePermissionTo($perm);
            $admin->givePermissionTo($perm);
            $employee->givePermissionTo($perm);
            $client->givePermissionTo($perm);
        }
    }
}
