<?php

namespace Database\Seeders;

use App\Models\Gallery;
use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'VFFlUiktUj8';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'QdATUpZoTl8';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = '7wltSrK39GY';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = '14PwZo9PwPs';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'QTPNuxPsZF0';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'irKgm_GYQEA';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = '6OHu0SpLPhg';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'Rethn02SQpU';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'tHLU7_5Nmb4';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'AnTedp07uWw';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'IYhkVz9E19A';
        $gallery->save();

        $gallery = new Gallery();
        $gallery->channel = 'youtube';
        $gallery->code = 'iqcpUAW5AKw';
        $gallery->save();
    }
}
