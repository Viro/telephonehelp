<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        //roles
        Permission::create(['name' => 'Aggiungi Ruolo', 'guard_name' => 'web', 'module_id' => 1]);
        Permission::create(['name' => 'Vedi Ruolo', 'guard_name' => 'web', 'module_id' => 1]);
        Permission::create(['name' => 'Modifica Ruolo', 'guard_name' => 'web', 'module_id' => 1]);
        Permission::create(['name' => 'Elimina Ruolo', 'guard_name' => 'web', 'module_id' => 1]);

        //permissions
        Permission::create(['name' => 'Aggiungi Permessi', 'guard_name' => 'web', 'module_id' => 2]);
        Permission::create(['name' => 'Vedi Permessi', 'guard_name' => 'web', 'module_id' => 2]);
        Permission::create(['name' => 'Modifica Permessi', 'guard_name' => 'web', 'module_id' => 2]);
        Permission::create(['name' => 'Elimina Permessi', 'guard_name' => 'web', 'module_id' => 2]);

        //users
        Permission::create(['name' => 'Aggiungi Utente', 'guard_name' => 'web', 'module_id' => 3]);
        Permission::create(['name' => 'Vedi Utente', 'guard_name' => 'web', 'module_id' => 3]);
        Permission::create(['name' => 'Modifica Utente', 'guard_name' => 'web', 'module_id' => 3]);
        Permission::create(['name' => 'Elimina Utente', 'guard_name' => 'web', 'module_id' => 3]);

        //pages
        Permission::create(['name' => 'Aggiungi Pagine', 'guard_name' => 'web', 'module_id' => 4]);
        Permission::create(['name' => 'Vedi Pagine', 'guard_name' => 'web', 'module_id' => 4]);
        Permission::create(['name' => 'Modifica Pagine', 'guard_name' => 'web', 'module_id' => 4]);
        Permission::create(['name' => 'Elimina Pagine', 'guard_name' => 'web', 'module_id' => 4]);

        //video
        Permission::create(['name' => 'Aggiungi Galleria', 'guard_name' => 'web', 'module_id' => 5]);
        Permission::create(['name' => 'Vedi Galleria', 'guard_name' => 'web', 'module_id' => 5]);
        Permission::create(['name' => 'Modifica Galleria', 'guard_name' => 'web', 'module_id' => 5]);
        Permission::create(['name' => 'Elimina Galleria', 'guard_name' => 'web', 'module_id' => 5]);

//        $super = Role::create([
//            'name' => 'superadmin'
//        ]);
//
//        $super->givePermissionTo($permission);



//        $permissions = Permission::orderBy('module_id')->pluck('name');
//        foreach($permissions as $perm){
//            $super->givePermissionTo("add_roles");
//        }

//        $super = new Role();
//        $super->name = 'superadmin';
//        $super->display_name = 'Superadmin'; // optional
//        $super->description = 'Può effettuare tutte le azioni nel sistema.'; // optional
//        $super->save();

//        $admin = new Role();
//        $admin->name = 'admin';
//        $admin->display_name = 'Amministratore'; // optional
//        $admin->description = 'Può effettuare tutte le azioni nel sistema come proprietario.'; // optional
//        $admin->save();
//
//        $employee = new Role();
//        $employee->name = 'employee';
//        $employee->display_name = 'Dipendente'; // optional
//        $employee->description = 'Il dipendente può effettuare solo alcune azioni.'; // optional
//        $employee->save();
//
//        $client = new Role();
//        $client->name = 'client';
//        $client->display_name = 'Cliente'; // optional
//        $client->description = 'Il cliente può effettuare solo alcune azioni.'; // optional
//        $client->save();


        // Assign admin Role
//        $user = User::where('email', '=', 'flauto.vincenzo84@gmail.com')->first();
//        $user->roles()->attach($super->id); // id only
//
//        // Assign permission
//        $permissions = Permission::orderBy('id', 'asc')->get();
//        foreach($permissions as $perm){
//            $super->attachPermission($perm);
//        }

//        sleep(5);

//        $roles = Role::all();
//        foreach($roles as $role){
////            var_dump($role->id);
//            $permissions = Permission::orderBy('module_id')->pluck('name');
//            foreach($permissions as $perm){
//                var_dump($perm);
//                $role->givePermissionTo($perm);
//            }
//        }
    }
}
