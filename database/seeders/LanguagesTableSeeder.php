<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            ['language_code' => 'en', 'language_name' => 'English', 'status' => 'disabled'],
        ];

        DB::table('language_settings')->insert($languages);
    }
}
