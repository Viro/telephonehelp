<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employee;
use Illuminate\Support\Facades\Hash;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = new Employee();
        $employee->name = 'Ciccio';
        $employee->surname = 'Pelliccia';
        $employee->cf = 'FFFFFF00F00F000C';
        $employee->company_id = 1;
        $employee->price = 2.5;
        $employee->save();
    }
}
