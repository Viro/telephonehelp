<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new Page();
        $page->title = 'Home';
        $page->slug = 'home';
        $page->meta_title = 'Home';
        $page->meta_description = 'Home';
        $page->description = 'Home';
        $page->save();

        $page = new Page();
        $page->title = 'Chi Siamo';
        $page->slug = 'chi-siamo';
        $page->meta_title = 'Chi Siamo';
        $page->meta_description = 'Chi Siamo';
        $page->description = 'Chi Siamo';
        $page->save();

        $page = new Page();
        $page->title = 'Servizi';
        $page->slug = 'servizi';
        $page->meta_title = 'Servizi';
        $page->meta_description = 'Servizi';
        $page->description = 'Servizi';
        $page->save();

        $page = new Page();
        $page->title = 'Galleria';
        $page->slug = 'galleria';
        $page->meta_title = 'Galleria';
        $page->meta_description = 'Galleria';
        $page->description = 'Galleria';
        $page->save();

        $page = new Page();
        $page->title = 'Cookie policy';
        $page->slug = 'cookie';
        $page->meta_title = 'Cookie policy';
        $page->meta_description = 'Cookie policy';
        $page->description = 'Cookie policy';
        $page->save();

        $page = new Page();
        $page->title = 'Privacy Policy';
        $page->slug = 'privacy';
        $page->meta_title = 'Privacy Policy';
        $page->meta_description = 'Privacy Policy';
        $page->description = 'Privacy Policy';
        $page->save();

        $page = new Page();
        $page->title = 'Contatti';
        $page->slug = 'contatti';
        $page->meta_title = 'Contatti';
        $page->meta_description = 'Contatti';
        $page->description = 'Contatti';
        $page->save();
    }
}
