<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Yajra\DataTables\DataTables;
use App\Helper\Reply;

class GalleriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['index','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['create','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['edit','update']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['destroy']]);

        $this->activeMenuGallery = true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        Auth::user()->can('Vedi Galleria');
        return view('galleries.index', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->can('Modifica Galleria');
        $this->gallery = Gallery::find($id);
        $this->roles = Role::pluck('name','name')->all();
        $this->userRole = Auth::user()->roles->pluck('name','name')->all();
        return view('galleries.edit',$this->data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->can('Modifica Galleria');
        $this->validate($request, [
//            'channel' => 'required',
            'code' => 'required',
        ]);
        $input = $request->all();
        $page = Gallery::find($id);
        $page->update($input);
        return redirect()->route('galleries.index')
            ->with('success',"Galleria modificata con successo");
    }

    /**
     * Return all pages
     *
     * @return mixed
     */
    public function data() {
        $pages = Gallery::orderBy('created_at', 'desc')->get();

        return Datatables::of($pages)
            ->addColumn('action', function($row){

                $link = '';

                if(Auth::user()->can('Modifica Galleria')) {
                    $link .= '<a href="' . route('galleries.edit', [$row->id]) . '" class="btn btn-sm btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="Modifica video"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a> ';
                }

                return $link;

            })
            ->editColumn(
                'code',
                function ($row) {
                    return $row->code;
                }
            )
            ->rawColumns(['code','action'])
            ->make(true);
    }
}
