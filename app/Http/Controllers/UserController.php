<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Yajra\DataTables\DataTables;
use App\Helper\Reply;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['index','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['create','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['edit','update']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['destroy']]);

        $this->activeMenuUsers = true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        Auth::user()->can('Vedi Utente');
        return view('users.index', $this->data);

//        $data = User::orderBy('id','DESC')->paginate(5);
//        return view('users.index',compact('data'))
//            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->can('Crea Utente');
        $this->roles = Role::pluck('name','name')->all();
        return view('users.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->can('Crea Utente');
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')
            ->with('success',__("app.userCreatedSuccess"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->can('Modifica Utente');
        $this->user = User::find($id);
        $this->roles = Role::pluck('name','name')->all();
        $this->userRole = $this->user->roles->pluck('name','name')->all();
        return view('users.edit',$this->data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->can('Modifica Utente');
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));
        }
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
        return redirect()->route('users.index')
            ->with('success',__("app.userUpdateSuccess"));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->can('Elimina Utente');
        $user = User::find((int)$id)->delete();
        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * Return all users
     *
     * @return mixed
     */
    public function data() {
        $users = User::orderBy('id', 'asc')->get();

        return Datatables::of($users)
            ->addColumn('action', function($row){

                $link = '';

                if(Auth::user()->can('Modifica Utente')) {
                    $link .= '<a href="' . route('users.edit', [$row->id]) . '" class="btn btn-sm btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="' . __("app.viewAdministrator") . '"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a> ';
                }

                if(Auth::user()->can('Elimina Utente')) {
                    $link .= '<a href="javascript:;" class="btn btn-sm btn-danger btn-circle sa-params"
                  data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="' . __("app.deleteAdministrator") . '"><i class="fa fa-times" aria-hidden="true"></i></a> ';
                }

                return $link;

            })
            ->editColumn(
                'name',
                function ($row) {
                    return ucfirst($row->name);
                }
            )
            ->editColumn(
                'surname',
                function ($row) {
                    return ucfirst($row->surname);
                }
            )
            ->editColumn(
                'email',
                function ($row) {
                    return '<a href="'.route('users.edit', $row->id).'">'.$row->email.'</a>';
                }
            )
            ->editColumn(
                'role',
                function ($row) {
                    $user = User::findOrFail($row->id)->getRoleNames();
                    return ucfirst($user);
                }
            )
            ->rawColumns(['name', 'surname', 'email', 'role', 'action'])
            ->make(true);
    }
}
