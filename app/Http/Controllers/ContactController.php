<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response' => 'required'
        ]);

        try {
            Contact::create($request->all());

            return redirect()->back()
                ->with(['success' => 'Grazie per averci contattato. Ti ricontatteremo il prima possibile.']);

//            $responseArray = array('type' => 'success', 'message' => 'Grazie per averci contattato. Ti ricontatteremo il prima possibile.');
//            return response()->json($responseArray);

        }catch (\Exception $e){
            dd($e->getMessage());
//            $responseArray = array('type' => 'danger', 'message' => $e->getMessage());
//            return response()->json($responseArray);
            return redirect()->back()
                ->with(['error' => 'Si è verificato un errore. Riprova più tardi']);
        }

    }
}
