<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['index','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['create','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['edit','update']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['destroy']]);

        $this->activeMenuRoles= true;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $roles = Role::orderBy('id','DESC')->paginate(5);
//        return view('roles.index',compact('roles'))
//            ->with('i', ($request->input('page', 1) - 1) * 5);
        return view('roles.index', $this->data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->permission = Permission::get();
        return view('roles.create',$this->data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('roles.index')
            ->with('success',__("app.roleCreatedSuccess"));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->role = Role::find($id);
        $this->rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();
        return view('roles.show',$this->data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->role = Role::find($id);
        $this->permission = Permission::get();
        $this->rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        return view('roles.edit',$this->data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('roles.index')
            ->with('success',__("app.roleUpdateSuccess"));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        DB::table("roles")->where('id',$id)->delete();
//        return redirect()->route('roles.index')
//            ->with('success','Role deleted successfully');
        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * Return all roles
     *
     * @return mixed
     */
    public function data() {
        $roles = Role::orderBy('id', 'asc')->get();

        return Datatables::of($roles)
            ->addColumn('action', function($row){

                $link = '';

                $link .= '<a href="' . route('roles.edit', [$row->id]) . '" class="btn btn-sm btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="' . __("app.viewRole") . '"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a> ';

                $link .= '<a href="javascript:;" class="btn btn-sm btn-danger btn-circle sa-params"
                  data-toggle="tooltip" data-role-id="' . $row->id . '" data-original-title="' . __("app.deleteRole") . '"><i class="fa fa-times" aria-hidden="true"></i></a> ';

                return $link;

            })
            ->editColumn(
                'name',
                function ($row) {
                    return ucfirst($row->name);
                }
            )
            ->editColumn(
                'email',
                function ($row) {
                    return '<a href="'.route('users.edit', $row->id).'">'.$row->email.'</a>';
                }
            )
            ->rawColumns(['name', 'action'])
            ->make(true);
    }
}
