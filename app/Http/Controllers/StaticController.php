<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class StaticController extends Controller
{

    public $url_website;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $client_id = Google::getClient();
//        dd($client_id);
        $this->url_website = env('APP_URL');
    }

    /**
     * Show the application home.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $reviews = array();
        $google_api_key = env('GOOGLE_API_KEY');
        $google_place_id = env('GOOGLE_PLACE_ID');

        $page = Page::where('slug', 'home')->first();
        $this->canonical = $this->url_website;
        $this->meta_title = $page->meta_title;
        $this->meta_description = $page->meta_description;

        $url = "https://maps.googleapis.com/maps/api/place/details/json?key=$google_api_key&placeid=$google_place_id&language=it";
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec ($ch);
        $res        = json_decode($result,true);

        if($res['result']){
            $reviews    = $res['result']['reviews'];
        }

//        dd($reviews);

        $this->reviews = $reviews;

        return view('front.index', $this->data);
    }

    /**
     * Show the application about us.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        $page = Page::where('slug', 'chi-siamo')->first();
        $this->canonical = route('about');
        $this->meta_title = $page->meta_title;
        $this->meta_description = $page->meta_description;
        return view('front.about', $this->data);
    }

    /**
     * Show the application services.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function services()
    {
        $page = Page::where('slug', 'servizi')->first();
        $this->canonical = route('services');
        $this->meta_title = $page->meta_title;
        $this->meta_description = $page->meta_description;
        return view('front.services', $this->data);
    }

    /**
     * Show the application galleries.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function gallery(Request $request)
    {
        $page = Page::where('slug', 'galleria')->first();
        $this->canonical = route('galleriesfront');
        $this->meta_title = $page->meta_title;
        $this->meta_description = $page->meta_description;
        $this->show_principal = true;
        if(isset($request->page) && $request->page > 1){
            $this->show_principal = false;
            $this->canonical = route('galleriesfront') . '?' . http_build_query($request->all());
        }else{
            $this->canonical = route('galleriesfront');
        }

        $this->galleries = Gallery::paginate(10);
        $this->galleries->appends(['sort' => 'created_at', 'desc']);
        return view('front.gallery', $this->data);
    }

    /**
     * Show the application contact.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contact(Request $request)
    {
        $page = Page::where('slug', 'contatti')->first();
        $this->canonical = route('contact');
        $this->meta_title = $page->meta_title;
        $this->meta_description = $page->meta_description;
        return view('front.contact', $this->data);
    }

    /**
     * Show the application cookies.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cookie(Request $request)
    {
        $page = Page::where('slug', 'cookie')->first();
        $this->canonical = route('cookie');
        $this->meta_title = $page->meta_title;
        $this->meta_description = $page->meta_description;
        $this->description = $page->description;
        return view('front.cookie', $this->data);
    }

    /**
     * Show the application privacy.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function privacy(Request $request)
    {
        $page = Page::where('slug', 'privacy')->first();
        $this->canonical = route('privacy');
        $this->meta_title = $page->meta_title;
        $this->meta_description = $page->meta_description;
        $this->description = $page->description;
        return view('front.privacy', $this->data);
    }
}
