<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Yajra\DataTables\DataTables;
use App\Helper\Reply;

class PagesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['index','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['create','store']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['edit','update']]);
        $this->middleware('permission:superadmin|admin|employee|client', ['only' => ['destroy']]);

        $this->activeMenuPages = true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        Auth::user()->can('Vedi Pagine');
        return view('pages.index', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->can('Modifica Pagine');
        $this->pages = Page::find($id);
        $this->roles = Role::pluck('name','name')->all();
        $this->userRole = Auth::user()->roles->pluck('name','name')->all();
        return view('pages.edit',$this->data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->can('Modifica Pagine');
        $this->validate($request, [
//            'title' => 'required',
            'meta_title' => 'required',
            'meta_description' => 'required',
//            'description' => 'required',
        ]);
        $input = $request->all();
        $page = Page::find($id);
        $page->update($input);
        return redirect()->route('pages.index')
            ->with('success',"Pagina modificata con successo");
    }

    /**
     * Return all pages
     *
     * @return mixed
     */
    public function data() {
        $pages = Page::orderBy('id', 'asc')->get();

        return Datatables::of($pages)
            ->addColumn('action', function($row){

                $link = '';

                if(Auth::user()->can('Modifica Pagine')) {
                    $link .= '<a href="' . route('pages.edit', [$row->id]) . '" class="btn btn-sm btn-info btn-circle"
                  data-toggle="tooltip" data-original-title="Modifica pagina"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a> ';
                }

                return $link;

            })
            ->editColumn(
                'title',
                function ($row) {
                    return ucfirst($row->title);
                }
            )
            ->rawColumns(['title','action'])
            ->make(true);
    }
}
